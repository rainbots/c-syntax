#include <stdio.h>

typedef struct
{
    int numbers[6];
}IntegerListA_s;

IntegerListA_s Initialize()
{ // This works.
    IntegerListA_s list = {
        {1, 2, 3, 4, 5, 6}
        };
    return list;
}

typedef struct
{
    int *integers;
}IntegerListP_s;

IntegerListP_s FailsToInitialize()
{ // This does not work (on purpose -- it's an example).
    int initial_values[] = {1, 2, 3, 4, 5, 6};
    IntegerListP_s list = {
        .integers=initial_values
        };
    return list;
}

int main()
{
    printf(
        "# What is the syntax to make a struct member an array?\n"
        "This is wrong:\n"
        "```c\n"
        "typedef struct\n"
        "{\n"
        "    int numbers[];  // wrong: need to specify array size\n"
        "}IntegerListA_s;\n"
        "```\n"
        );
    printf(
        "clang throws this compiler error:\n"
        "```make\n"
        "src/arrays-in-structs.c|5 col 9| error: flexible array member 'numbers' not allowed in otherwise empty struct\n"
        "||     int numbers[];\n"
        "||         ^\n"
        "|| 1 error generated.\n"
        "```\n"
        );
    printf(
        "Give the array size:\n"
        "```c\n"
        "typedef struct\n"
        "{\n"
        "    int numbers[6];  // this is right\n"
        "}IntegerListA_s;\n"
        "```\n"
        );
    printf(
        "This is true in general, not just for struct members, though the error message is slightly different.\n"
        );
    printf(
        "```c\n"
        "int main()\n"
        "{\n"
        "    ...\n"
        "    int array_needs_a_size[];  // wrong\n"
        "    ...\n"
        "}\n"
        "```\n"
        );
    printf(
        "Attempting to define an array without a size at *function scope* is a compiler error. Here is the clang error for declaring an array without a size at *function scope*:\n"
        "```make\n"
        "src/arrays-in-structs.c|43 col 9| error: definition of variable with array type needs an explicit size or an initializer\n"
        "||     int array_needs_a_size[];\n"
        "||         ^\n"
        "|| 1 error generated.\n"
        "```\n"
        );
    printf(
        "Attempting to define an array without a size at *file scope* is a compiler warning. Here is the clang warning for declaring an array without a size at *file scope*:\n"
        "```make\n"
        "src/arrays-in-structs.c|7 col 5| warning: tentative array definition assumed to have one element\n"
        "|| int array_needs_a_size[];\n"
        "||     ^\n"
        "|| 1 warning generated.\n"
        "```\n"
        );

    printf(
        "\n---\n"
        "## Array size cannot be a variable.\n"
        "\nThe array size must be known at compile time, otherwise the array is considered *variable length* and clang throws a compiler warning:\n"
        "```make\n"
        "lib/test/test_bsort.c|37 col 18| warning: variable length array folded to constant array as an extension [-Wgnu-folding-constant]\n"
        "||     int unsorted[MaxListLength];\n"
        "||                  ^~~~~~~~~~~~~\n"
        "```\n"
        );
    printf(
        "\nThe same warning is generated whether the array is in a struct or not, and whether the array is declared at file scope or at function scope. Here is the function scope version:\n"
        "```c\n"
        "int const array_size = 3;\n"
        "int a_variable_array[array_size]; // wrong: warning\n"
        "```\n"
        "Use a `#define` to remove the warning.\n"
        "```c\n"
        "#define array_size 3\n"
        "int not_a_variable_array[array_size]; // right\n"
        "```\n"
        );


    printf(
        "# What is the syntax to make a struct member a pointer?\n"
        "This is similar, but there is no size specification.\n"
        "```c\n"
        "typedef struct\n"
        "{\n"
        "int *integers;\n"
        "}IntegerListP_s;\n"
        "```\n"
        );

    printf(
        "# What is the syntax to initialize an array with a "
        "designated initializer?\n"
        );
    int integers[] = {1, 2, 3};
    printf(
        "## Initializing an array with `{}` works in clang and g++\n"
        );
    printf(
        "Initialize an array:\n"
        "```c\n"
        "int integers[] = {1, 2, 3};\n"
        "```\n"
        "Access the first element:\n\n"
        "---\n"
        "integers[0]: %d\n", integers[0]
        );

    printf(
        "# What is the syntax to initialize a pointer with a "
        "designated initializer?\n"
        );
    printf(
        "## Can I initialize a pointer the same way? No. "
        "Cast as an array (C only).\n"
        );
    int *intptr = integers;
    printf(
        "```c\n"
        "int *intptr = {1, 2, 3};  // this is wrong\n"
        "```\n"
        );
    printf(
        "\nMake issues two warnings:\n"
        "```make\n"
        "/src/arrays-in-structs.c|30 col 20| warning: incompatible integer to pointer conversion initializing 'int *' with an expression of type 'int' [-Wint-conversion]\n"
        "||     int *intptr = {1, 2, 3};\n"
        "||                    ^\n"
        "/cygdrive/c/chromation-dropbox/Dropbox/c/TddFramework/TestDrive/kata/c-syntax/src/arrays-in-structs.c|30 col 23| warning: excess elements in scalar initializer\n"
        "||     int *intptr = {1, 2, 3};\n"
        "||                       ^\n"
        "|| 2 warnings generated.\n"
        "```\n"
        );
    printf(
        "\nThe first warning tells me the compiler views `{1, 2, 3}` "
        "as an `int` rather than an *array* of `int`.\n"
        "The second warning really drives home the point that "
        "clang was expecting the *rhs* to be a single value.\n"
        );
    printf(
        "\nSolution: cast the literal as an array of int:\n"
        "```c\n"
        "int *intptr = (int[]){1, 2, 3};\n"  // C only
        "```\n"
        );
    printf(
        "\nAccess the first element:\n\n"
        "---\n"
        "*intptr: %d\n", *intptr
        );
    printf(
        "\nThis works too:\n\n"
        "---\n"
        "intptr[0]: %d\n", intptr[0]
        );
    printf(
        "## Casting as an array does not work in C++. "
        "Make an array first, then assign the pointer to it.\n"
        );
    printf(
        "```c\n"
        "int *intptr = (int[]){1, 2, 3};\n"  // g++ error
        "```\n"
        );
    printf(
        "g++ throws `error: taking address of temporary array`. "
        "And of course the executable does not build.\n"
        "```make\n"
        "src/arrays-in-structs.c|31 col 34| warning: ISO C++ forbids compound-literals [-Wpedantic]\n"
        "||      int *intptr = (int[]){1, 2, 3};\n"
        "||                                   ^\n"
        "/cygdrive/c/chromation-dropbox/Dropbox/c/TddFramework/TestDrive/kata/c-syntax/src/arrays-in-structs.c|31 col 26| error: taking address of temporary array\n"
        "||      int *intptr = (int[]){1, 2, 3};\n"
        "||                           ^~~~~~~~~\n"
        "```\n"
        );
    printf(
        "To make this work in C and C++, "
        "create an array first to allocate automatic memory. "
        "Then assign the pointer.\n"
        "```c\n"
        "int integers[] = {1, 2, 3};\n"
        "int *intptr = integers;\n"  // Works for C/C++.
        "```\n"
        );
    printf(
        "## Does any of this change if the pointer is a `struct` member? "
        "No :)\n"
        );
    printf(
        "At file scope, I declare the `typedef`:\n"
        "```c\n"
        "typedef struct\n"
        "{\n"
        "    int *integers;\n"
        "}IntegerListP_s;\n"
        "```\n"
        "And I attempt to initialize the struct in the manner that "
        "initializing the pointer to an array worked in C/C++:\n"
        "```c\n"
        "// Works for C/C++.\n"
        "// g++ issues a designated initializer warning as expected.\n"
        "int integers[] = {1, 2, 3};\n"
        "IntegerListP_s list = {.integers=integers};\n"
        "```\n"
        );
    IntegerListP_s list = {.integers=integers};
    printf(
        "\nAs expected from the test in [struct-init][si], "
        "g++ gives the warning that it doesn't like "
        "designated initializers, but the executable still builds "
        "under clang and g++ and produces the same output.\n"
        "[si]: https://bitbucket.org/rainbots/c-syntax/src/master/build/struct-init.md\n"
        "```make\n"
        "/src/arrays-in-structs.c|111 col 26| warning: ISO C++ does not allow C99 designated initializers [-Wpedantic]\n"
        "||      IntegerListP_s list = {.integers=integers};\n"
        "||                           ^\n"
        "```\n"
        );
    printf(
        "\nAccess the first element of `list.integers`:\n\n"
        "---\n"
        );
    printf(
        "list.integers[0]: %d\n", list.integers[0]
        );

    printf(
        "# Struct member as *pointer* vs *array* matters when copying the struct\n"
        "A great use of structs is returning a `struct` from a function. This is a common trick for returning arrays. The `struct` contents are copied when returning, whereas a pointer is copied, but whatever it pointed to is trashed. An array cannot be returned on its own, but it can be returned as part of a `struct`.\n\n"

        "Returning this `struct` from a function returns the array value assigned in the function:\n"
        "```c\n"
        "typedef struct\n"
        "{\n"
        "    int numbers[6];\n"
        "}IntegerListA_s;\n"
        "\n"
        "IntegerListA_s Initialize()\n"
        "{ // This works.\n"
        "    IntegerListA_s list = {\n"
        "        {1, 2, 3, 4, 5, 6}\n"
        "        };\n"
        "    return list;\n"
        "}\n"
        "```\n"
        );
    IntegerListA_s good_list = Initialize();
    printf(
        "```c\n"
        "int main()\n"
        "{\n"
        "...\n"
        "IntegerListA_s good_list = Initialize();\n"
        "...\n"
        "}\n"
        "```\n"
        );
    printf(
        "\n`good_list` returned from `Initialize()`:\n"
        "\n---\n"
        "```c\n"
        "{%d, %d, %d, %d, %d, %d}\n"
        "```\n",
        good_list.numbers[0],
        good_list.numbers[1],
        good_list.numbers[2],
        good_list.numbers[3],
        good_list.numbers[4],
        good_list.numbers[5]
        );

    printf(
        "\nThis does not extend to `struct` pointer members. A `struct` pointer is treated as a pointer: the address is copied, but whatever it points to is trashed. Copying a struct and retaining the values of what its pointer members point to requires making a *deep* copy. The `=` is just a shallow copy. "
        "Here is the same code but with a pointer member. The function creates an array and then initializes the pointer member to point at the array. The extra step is required for compiling with g++ without error. But the array pointed to the by `struct` pointer member does not contain those initial values when the `struct` is returned. Instead, a `bad_list` of random garbage values is at those addresses:\n"
        "```c\n"
        "typedef struct\n"
        "{\n"
        "    int *integers;\n"
        "}IntegerListP_s;\n"
        "\n"
        "IntegerListP_s FailsToInitialize()\n"
        "{ // This does not work (on purpose -- it's an example).\n"
        "    int initial_values[] = {1, 2, 3, 4, 5, 6};\n"
        "    IntegerListP_s list = {\n"
        "        .integers=initial_values\n"
        "        };\n"
        "    return list;\n"
        "}\n"
        "```\n"
        );
    
    IntegerListP_s bad_list = FailsToInitialize();
    printf(
        "```c\n"
        "int main()\n"
        "{\n"
        "...\n"
        "IntegerListP_s bad_list = FailsToInitialize();\n"
        "...\n"
        "}\n"
        "```\n"
        );
    printf(
        "\n`bad_list` returned from `FailsToInitialize()`:\n"
        "\n---\n"
        "```c\n"
        "{%d, %d, %d, %d, %d, %d}\n"
        "```\n",
        bad_list.integers[0],
        bad_list.integers[1],
        bad_list.integers[2],
        bad_list.integers[3],
        bad_list.integers[4],
        bad_list.integers[5]
        );
}
