#include <stdio.h>

#define NumberOfMembers 3
typedef struct
{
    int a;
    int b;
    int c;
} ThreeInts_s;

int main()
{
    printf(
        "Table of contents:\n---\n"
        "- [Iterating a struct: don't.]"
        "(#markdown-header-iterating-a-struct:-don't.)\n"
        "    - [Access members by name]"
        "(#markdown-header-access-members-by-name)\n"
        "    - [No language support to access members]"
        "(#markdown-header-no-language-support-to-access-members)\n"
        "    - [Pointer arithmetic subtelty]"
        "(#markdown-header-pointer-arithmetic-subtelty)\n"
        "\n"
        ); //---e-n-d---
    printf(
        "# Iterating a struct: don't.\n"
        "TLDR: An object needs a *copy* function. "
        "An object is implemented as a struct "
        "(unless it is a *singleton*). "
        "Copy a struct with `=`.\n"
        "```c\n"
        "SomeStruct_s copy = original;\n"
        "```\n"
        "I'm dealing with a pointer to a struct, but it "
        "is the same. "
        "Copy a pointer to struct with `=` and "
        "dereference the pointer. First the pointer "
        "needs to be defined, then it can be "
        "dereferenced for the copy. The definition "
        "is different for automatic vs. manual "
        "memory management.\n\n"
        "Using automatic memory management:\n"
        "```c\n"
        "SomeStruct_s *copy;  // automatic or static\n"
        "```\n"
        "Using manual memory management (allocating "
        "memory on the heap to be manually deallocated "
        "later:\n"
        "```c\n"
        "SomeStruct_s *copy = (SomeStruct_s *)"
        "malloc(sizeof(SomeStruct_s));  "
        "// manual version\n"
        "```\n"
        "And the copy uses the `=` just like before:\n"
        "```c\n"
        "*copy = *original;\n"
        "```\n"
        "Using `=` is a *shallow copy*. A "
        "*shalllow copy* "
        "is sufficient if the struct contains no "
        "pointers. "
        "If it does contain pointers, the *copy* "
        "function has to dereference each pointer "
        "to make a *deep copy*. This is the motivation "
        "for finding a way to iterate over a struct "
        "by member, but it turns out to be a messy "
        "thing people do not routinely do. "
        "Wisdom differs. Everyone agrees that "
        "`offest_of()` is the right approach. "
        "[Wikipedia warns of **edge cases**][wiki-fud], "
        "while [Nigel Jones uses it all the time]"
        "[nj-article]. "
        "At the end I outline how it could be done.\n"
        "[wiki-fud]: https://en.wikipedia.org/"
        "wiki/Offsetof#Limitations\n"
        "[nj-article]: https://barrgroup.com/"
        "Embedded-Systems/How-To/C-Offsetof-Macro"
        "\n\n"
        "---\n\n"
        );
    printf(
        "## Access members by name\n"
        );
    ThreeInts_s init = {.a=1, .b=2, .c=3};
    printf(
        "Here is a struct:\n\n"
        "=====[ file scope ]=====\n"
        "```c\n"
        "typedef struct\n"
        "{\n"
        "    int a;\n"
        "    int b;\n"
        "    int c;\n"
        "} ThreeInts_s;\n"
        "```\n"
        );
    printf(
        "The struct is initialized with a designated initializer:\n\n"
        "```c\n"
        "int main()\n"
        "{\n"
        "    ...\n"
        "    ThreeInts_s init = {.a=1, .b=2, .c=3};\n"
        "    ...\n"
        "}\n"
        "```\n"
        );
    printf(
        "The struct members are accessible by name:\n\n"
        "`init.a`:%d, "
        "`init.b`:%d, "
        "`init.c`:%d\n\n",
        init.a, init.b, init.c
        );
    printf(
        "## No language support to access members "
        "by index.\n"
        );
    printf(
        "There is no index notation "
        "like for an array. And there is no language "
        "feature to query the number of members.\n\n"
        );
    printf(
        "To access struct members by iterating in a "
        "loop, I need to know the number of members in "
        "the struct. Does `sizeof()` tell me this? "
        "No:\n\n"
        );
    printf(
        "`sizeof(init)`: %zu\n\n", sizeof(init)
        );
    printf(
        "## Code struct indexing yourself. But how?\n"
        );
    printf(
        "`sizeof()` tells me the *number of bytes*, not "
        "the number of members. Hardcode the number "
        "of members as a macro:\n\n"
        "```c\n"
        "#define NumberOfMembers 3\n"
        "```\n"
        );
    printf(
        "Providing a member's index for looping is "
        "much harder."
        "\n\n---\n"
        "The struct has an address. "
        "The structs I care about are on "
        "the heap, so this is a heap address. "
        "The address of the struct name itself "
        "is the address of the first member in the "
        "struct. Access subsequent struct members "
        "by a `base-plus-offset` approach. "
        "The idea is to calculate "
        "the address of some member since you know "
        "the address of the first member, the index "
        "of this member, and the size in bytes "
        "of every member.\n\n"
        );
    printf(
        "The only hitch in the calculation is the "
        "system-dependent padding. That is why "
        "there is a library function that does "
        "this calculation given the struct typedef "
        "and the name of the member in question. "
        "The function is `offsetof()` in `stddef.h`. "
        "[Here is an article about offsetof]"
        "[offsetof-article]. "
        "The function treats the address of the first "
        "member as zero, so the *real* address is "
        "the value returned by `offsetof` plus "
        "the base address of the struct. Here are "
        "the offsets for the three members of a "
        "ThreeInts_s: \n"
        "[offsetof-article]: https://barrgroup.com/"
        "Embedded-Systems/How-To/C-Offsetof-Macro"
        "\n\n"
        );
    printf(
        "`offsetof(ThreeInts_s, a)`: %lu,\n\n"
        "`offsetof(ThreeInts_s, b)`: %lu,\n\n"
        "`offsetof(ThreeInts_s, c)`: %lu\n\n",
        offsetof(ThreeInts_s, a),
        offsetof(ThreeInts_s, b),
        offsetof(ThreeInts_s, c)
        );
    printf(
        "And get the address on the heap of the first "
        "struct member with `&`:\n\n"
        //"`(size_t)&init`: %zu\n\n", (size_t)&init
        "`(uintptr_t)&init`: %zu\n\n", (uintptr_t)&init
        );
    printf(
        "Here are the addresses of the three members:\n\n"
        "`(uintptr_t)&init + offsetof(ThreeInts_s, a)`: "
        "%lu,\n\n"
        "`(uintptr_t)&init + offsetof(ThreeInts_s, b)`: "
        "%lu,\n\n"
        "`(uintptr_t)&init + offsetof(ThreeInts_s, c)`: "
        "%lu\n\n",
        (uintptr_t)&init + offsetof(ThreeInts_s, a),
        (uintptr_t)&init + offsetof(ThreeInts_s, b),
        (uintptr_t)&init + offsetof(ThreeInts_s, c)
        );
    printf(
        "Sanity-check this matches the address obtained "
        "with the simple `&` operator on `init.a`:\n\n"
        "`           (void *)&(init.a)`: %p\n\n" // %p
        "`(uintptr_t)(void *)&(init.a)`: %lu\n\n" // %p
        "`        (uintptr_t)&(init.a)`: %lu\n\n"
        "`(uintptr_t)&init + offsetof(ThreeInts_s, a)`: "
        "%lu,\n\n",
        (void *)&(init.a),
        (uintptr_t)(void *)&(init.a),
        (uintptr_t)&(init.a),
        (uintptr_t)&init + offsetof(ThreeInts_s, a)
        );
    int *a_addr = (int *)(uintptr_t)&init + offsetof(ThreeInts_s, a)/sizeof(int);
    int *b_addr = (int *)(uintptr_t)&init + offsetof(ThreeInts_s, b)/sizeof(int);
    int *c_addr = (int *)(uintptr_t)&init + offsetof(ThreeInts_s, c)/sizeof(int);
    //int *a_addr = &(init.a);
    printf(
        "Having calculated the address, access the "
        "value.\n"
        "```c\n"
        "int *a_addr = (int *)(uintptr_t)&init + "
            "offsetof(ThreeInts_s, a)/sizeof(int);\n"
        "int *b_addr = (int *)(uintptr_t)&init + "
            "offsetof(ThreeInts_s, b)/sizeof(int);\n"
        "int *c_addr = (int *)(uintptr_t)&init + "
            "offsetof(ThreeInts_s, c)/sizeof(int);\n"
        "```\n"
        "*a_addr: %d\n\n"
        "*b_addr: %d\n\n"
        "*c_addr: %d\n\n",
        *a_addr, *b_addr, *c_addr
        );
    printf(
        "Putting all of the above together, "
        "associate a member index with a byte offset, "
        "then add it to the base address, "
        "then dereference it. There is a subtelty in "
        "the pointer arithmetic in the units. This is "
        "why the offset is divided by the `sizeof(int)`."
        "\n\n"
        );
    printf(
        "## Pointer arithmetic subtelty\n"
        );
    uintptr_t offsets[3];
    offsets[0] = offsetof(ThreeInts_s, a);
    offsets[1] = offsetof(ThreeInts_s, b);
    offsets[2] = offsetof(ThreeInts_s, c);
    printf(
        "Here are the offsets and the expected values "
        "at each address:\n"
        "```c\n"
        "uintptr_t offsets[3];\n"
        "offsets[0] = offsetof(ThreeInts_s, a);\n"
        "offsets[1] = offsetof(ThreeInts_s, b);\n"
        "offsets[2] = offsetof(ThreeInts_s, c);\n"
        "```\n"
        );
    printf(
        "offsets[0]: %lu bytes, init.a: %d\n\n"
        "offsets[1]: %lu bytes, init.b: %d\n\n"
        "offsets[2]: %lu bytes, init.c: %d\n\n",
        offsets[0], init.a,
        offsets[1], init.b,
        offsets[2], init.c
        );
    printf(
        "## Careful with casts in the address arithmetic."
        "\n\n---\n"
        "Casting base as `(int *)base` does not work! "
        "`a` has the right value, but `b` "
        "and `c` do not:\n\n"
        );
    uintptr_t base = (uintptr_t)&init;
    int *pa = (int *)base + offsets[0];
    int *pb = (int *)base + offsets[1];
    int *pc = (int *)base + offsets[2];
    printf(
        "```c\n"
        "uintptr_t base = (uintptr_t)&init;\n"
        "int *pa = (int *)base + offsets[0];\n"
        "int *pb = (int *)base + offsets[1];\n"
        "int *pc = (int *)base + offsets[2];\n"
        "```\n"
        );
    printf(
        "offsets[0]: %lu, *pa: %d\n\n"
        "offsets[1]: %lu, *pb: %d\n\n"
        "offsets[2]: %lu, *pc: %d\n\n",
        offsets[0], *pa,
        offsets[1], *pb,
        offsets[2], *pc
        );
    printf(
        "Blindly thinking `(void *)` solves everything, "
        "swapping out `(int *)base` for `(void *)` "
        "seems to work, but do not do this. "
        "\n\n---\n"
        );
    pa = (void *)base + offsets[0];
    pb = (void *)base + offsets[1];
    pc = (void *)base + offsets[2];
    printf(
        "```c\n"
        "uintptr_t base = (uintptr_t)&init;\n"
        "int *pa = (void *)base + offsets[0];\n"
        "int *pb = (void *)base + offsets[1];\n"
        "int *pc = (void *)base + offsets[2];\n"
        "```\n"
        );
    printf(
        "offsets[0]: %lu, *pa: %d\n\n"
        "offsets[1]: %lu, *pb: %d\n\n"
        "offsets[2]: %lu, *pc: %d\n\n",
        offsets[0], *pa,
        offsets[1], *pb,
        offsets[2], *pc
        );
    printf(
        "Casting base as a pointer "
        "to void only works because there is "
        "a GNU extension to treat `(void *)` as "
        "`(char *)` for pointer aritmetic. Normally "
        "pointer arithmetic is prohibited for a "
        "pointer to void. Here is the compiler "
        "warning about this:\n\n"
        );
    printf(
        "```make\n"
        "src/struct-iterating.c|267 col 23| warning: "
        "arithmetic on a pointer to void is a "
        "GNU extension [-Wpointer-arith]\n"
        "||     pa = (void *)base + offsets[0];\n"
        "||          ~~~~~~~~~~~~ ^\n"
        "```\n"
        );

    pa = (uint8_t *)base + offsets[0];
    pb = (uint8_t *)base + offsets[1];
    pc = (uint8_t *)base + offsets[2];
    printf(
        "We are now operating at the bytes level. "
        "We know that base holds a number in units of "
        "bytes and that our offsets are in units of "
        "bytes. So the address to point to is "
        "calculated in units of bytes. \n"
        "```c\n"
        "pa = (uint8_t *)base + offsets[0];\n"
        "pb = (uint8_t *)base + offsets[1];\n"
        "pc = (uint8_t *)base + offsets[2];\n"
        "```\n"
        );
    printf(
        "offsets[0]: %lu, *pa: %d\n\n"
        "offsets[1]: %lu, *pb: %d\n\n"
        "offsets[2]: %lu, *pc: %d\n\n",
        offsets[0], *pa,
        offsets[1], *pb,
        offsets[2], *pc
        );
    printf(
        "This is physically correct, but we have "
        "confused the compiler. "
        "Casting `base` as a `(int *)` to make the "
        "compiler happy is completely wrong. "
        "The number returned "
        "by offsets is in *bytes*, not *ints*. "
        "The compiler "
        "assumes the units are ints. For `init.a` this "
        "does not matter because the offset is 0 and 0 "
        "bytes equals 0 ints. But for non-zero offset, "
        "we are pointing to the completely wrong place, "
        "in fact, `init.b` isn't even inside the `init` "
        "memory space: it is one `int` beyond.\n\n"
        "`(uint8_t *)` is the correct casting, "
        "but it throws an incompatible types message:"
        "\n"
        );
    printf(
        "```make\n"
        "src/struct-iterating.c|224 col 10| warning: "
        "incompatible pointer types initializing 'int *' "
        "with an expression of type 'uint8_t *' "
        "(aka 'unsigned char *') "
        "[-Wincompatible-pointer-types]\n"
        "||     int *pa = (uint8_t *)base + offsets[0];\n"
        "||          ^    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
        "```\n"
        "What is to be done to silence the warning? "
        "Our pointer has to be declared as a "
        "pointer to an int because when we dereference "
        "it, the compiler needs to know how many bytes "
        "make up its value. We can go back to casting "
        "base as a pointer to int but do math on the "
        "offset to put it in units of `int`.\n\n"
        );
    pa = (int *)base + offsets[0]/sizeof(int);
    pb = (int *)base + offsets[1]/sizeof(int);
    pc = (int *)base + offsets[2]/sizeof(int);
    printf(
        "```c\n"
        "pa = (int *)base + offsets[0]/sizeof(int);\n"
        "pb = (int *)base + offsets[1]/sizeof(int);\n"
        "pc = (int *)base + offsets[2]/sizeof(int);\n"
        "```\n"
        );
    printf(
        "offsets[0]: %lu, *pa: %d\n\n"
        "offsets[1]: %lu, *pb: %d\n\n"
        "offsets[2]: %lu, *pc: %d\n\n",
        offsets[0], *pa,
        offsets[1], *pb,
        offsets[2], *pc
        );

    printf(
        "The only hitch is that calculating the "
        "address requires knowing the datatype. "
        "To avoid this, cast the `lhs` as void "
        "pointer?\n\n"
        );
    void *vpa = (uint8_t *)base + offsets[0];
    void *vpb = (uint8_t *)base + offsets[1];
    void *vpc = (uint8_t *)base + offsets[2];
    printf(
        "```c\n"
        "void *vpa = (uint8_t *)base + offsets[0];\n"
        "void *vpb = (uint8_t *)base + offsets[1];\n"
        "void *vpc = (uint8_t *)base + offsets[2];\n"
        "```\n"
        );
    printf(
        "offsets[0]: %lu, `*(int *)vpa`: %d\n\n"
        "offsets[1]: %lu, `*(int *)vpb`: %d\n\n"
        "offsets[2]: %lu, `*(int *)vpc`: %d\n\n",
        offsets[0], *(int *)vpa,
        offsets[1], *(int *)vpb,
        offsets[2], *(int *)vpc
        );
    printf(
        "Yes! That works. And it makes the pointer "
        "arithmetic simpler without the divide by "
        "sizeof() step. But we still need to know the "
        "datatype when the pointer is dereferenced.\n\n"
        );
    printf(
        "Lastly, I am too tired to write out the "
        "mechanics, but using the above code examples, "
        "use `offsetof()` to setup an array of "
        "*offset strides* to index in the for loop.\n"
        //"```c\n"
        //"```\n"
        );
    printf("\n\n---\n");
    printf(
        "Conclusion: this is a lot of work and "
        "is maybe a sign I'm doing something stupid. "
        "Is the deep copy without naming elements "
        "really necessary?\n\n"
        );
}
