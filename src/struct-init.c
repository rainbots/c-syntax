#include <stdio.h>

typedef struct
{
    int a;
}IntegerList_s;

int main()
{
    printf("# What is the syntax to initialize a struct?\n");
    printf(
        "The real question ended up being\n"
        "*Does C++ support designated initializers?* No.\n"
        );
    printf(
        "## A simple `struct` type definition:\n"
        "```c\n"
        "typedef struct\n"
        "{\n"
        "    int a;\n"
        "}IntegerList_s;\n"
        "```\n"
        );
    IntegerList_s list = {.a=1};
    printf(
        "## This works in clang:\n"
        "\nDefine and initialize `list`:\n"
        "```c\n"
        "IntegerList_s list = {.a=1};\n"
        "```\n"
        );
    printf("`list.a`: %d\n", list.a);
    printf(
        "## But g++ issues a warning:\n"
        "```make\n"
        "src/struct-init.c|12 col 26| warning: ISO C++ does not allow C99 designated initializers [-Wpedantic]\n"
        "||      IntegerList_s list = {.a=1};\n"
        "||                           ^\n"
        "```\n"
        );
    printf(
        "This is not a shock.\n"
        "C supports designated initializers.\n"
        "C++ does not.\n"
        "But somehow it only triggers a warning, so I can still compile in C++.\n"
        "Ben Klemens [stands behind designated initializers][bk-des-inits-rock]:\n"
        "[bk-des-inits-rock]: https://modelingwithdata.org/pdfs/030-c_v_cpp.pdf\n"
        );
    printf("\n");
    printf(
        "> On the other side, the ISO added a few features to C a decade ago. The most notable\n"
        "> for me is designated initializers; I’ve written several entries here about how much you\n"
        "> can get out of this syntactic tweak. However, C++ has no intention of supporting\n"
        "> them. This author3\n"
        "> feels the rationale paper for not using designated initializers gives\n"
        "> “arguments that aren’t very convincing”, and I’d agree.\n"
        );
}
