# Table of Contents
- [Invoking](#markdown-header-invoking)
    - [Vim shortcut](#markdown-header-vim-shortcut)
    - [Make from the terminal](#markdown-header-make-from-the-terminal)
- [Viewing](#markdown-header-viewing)
- [Links to c-syntax docs](#markdown-header-links-to-c-syntax-docs)
- [Links to c-articles](#markdown-header-links-to-c-articles)

---e-n-d---
# Invoking
---
Invoke by Vim shortcut or by manually entering the `make` command at the
terminal.

## Vim shortcut
---
`{file-stem}` is passed in by:

    ;mrc / ;mrk / ;mr+

These commands:

* build the markdown from the .c source
* then open the markdown in a Vim window.

`;r<Space>` closes the markdown window (invoked from `.c` or `.md` window).

The last letter indicates the compiler:

- `c` - gcc
- `l` - clang
- `+` - g++

Alternatively, `{file-stem}` is passed in by:

    ;mkc / ;mkl / ;mk+

These commands just build the markdown. Invoke `;re` to open the markdown in a Vim window (invoked from the `.c` window, and the NERDTree must be at the project root where the `build` folder is visible).

## Make from the terminal
---
Alternatively, manually invoke make from the terminal. At the Vim command line:
```vim
:!make compiler=clang file-stem=%:t:r
```
Or `:terminal` (shortcut `<F2>`) and enter the same:
```bash
:!make compiler=clang file-stem=%:t:r
```

- `%` is the current Vim file
- `t` removes the path
- `r` removes the extension

Invoke `make` with the `-n` flag to preview targets/recipes with arguments `compiler` and `file-stem` expanded.

# Viewing
---
Instead of the Vim window (`;re` and `;r<Space>`), view the markdown with *Markdown Viewer*.

Open the markdown with `;re`. From the markdown window:
```vim
;pp
"+P
;cw
```
`;pp` gets the markdown file path with:
```vim
:let @+ = expand("%:p")
```
`"+P` pastes from the clipboard and leaves the cursor on the path text. `;cw`
invokes `cypgath -w` to convert the POSIX path to a Windows path. The Windows
path is now in the clipboard. From a PowerShell terminal:
```powershell
chrome "{paste_path_from_clipboard}"
```
Then just Alt-Tab to Chrome and `F5` to refresh each time the markdown is
built.

# Links to c-syntax docs
- [struct-init *C++ does not support designated 
  initializers*][struct-init]
- [arrays-in-structs *Struct member is a pointer,
  designated initializer uses an array not a
  `{}` literal*][arrays-in-structs]
- [struct-iterating *Do not try to loop over a struct,
  just access the members by name*][struct-iterating]

[struct-init]:       https://bitbucket.org/rainbots/c-syntax/src/master/build/struct-init.md

[arrays-in-structs]: https://bitbucket.org/rainbots/c-syntax/src/master/build/arrays-in-structs.md

[struct-iterating]:  https://bitbucket.org/rainbots/c-syntax/src/master/build/struct-iterating.md


# Links to c-articles
- [The Development of the C Language (1993)][c-history]
- [SINGLETON - the anti-pattern!][kh-singleton]

[kh-singleton]: https://accu.org/index.php/articles/337

[c-history]: http://www.bell-labs.com/usr/dmr/www/chist.html
