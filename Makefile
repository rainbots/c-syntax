file-in-Vim-window: build/${file-stem}.md

CFLAGS = -g -Wall -Wextra -pedantic

build/%.md: build/%.exe
	./$^ > $@

build/%.exe: src/%.c
	${compiler} $^ -o $@ ${CFLAGS}

clean-all-builds:
	rm -f build/struct-iterating.md
#	rm -f build/struct-init.md
#	rm -f build/arrays-in-structs.md
