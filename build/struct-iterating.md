Table of contents:
---
- [Iterating a struct: don't.](#markdown-header-iterating-a-struct:-don't.)
    - [Access members by name](#markdown-header-access-members-by-name)
    - [No language support to access members](#markdown-header-no-language-support-to-access-members)
    - [Pointer arithmetic subtelty](#markdown-header-pointer-arithmetic-subtelty)

# Iterating a struct: don't.
TLDR: An object needs a *copy* function. An object is implemented as a struct (unless it is a *singleton*). Copy a struct with `=`.
```c
SomeStruct_s copy = original;
```
I'm dealing with a pointer to a struct, but it is the same. Copy a pointer to struct with `=` and dereference the pointer. First the pointer needs to be defined, then it can be dereferenced for the copy. The definition is different for automatic vs. manual memory management.

Using automatic memory management:
```c
SomeStruct_s *copy;  // automatic or static
```
Using manual memory management (allocating memory on the heap to be manually deallocated later:
```c
SomeStruct_s *copy = (SomeStruct_s *)malloc(sizeof(SomeStruct_s));  // manual version
```
And the copy uses the `=` just like before:
```c
*copy = *original;
```
Using `=` is a *shallow copy*. A *shalllow copy* is sufficient if the struct contains no pointers. If it does contain pointers, the *copy* function has to dereference each pointer to make a *deep copy*. This is the motivation for finding a way to iterate over a struct by member, but it turns out to be a messy thing people do not routinely do. Wisdom differs. Everyone agrees that `offest_of()` is the right approach. [Wikipedia warns of **edge cases**][wiki-fud], while [Nigel Jones uses it all the time][nj-article]. At the end I outline how it could be done.
[wiki-fud]: https://en.wikipedia.org/wiki/Offsetof#Limitations
[nj-article]: https://barrgroup.com/Embedded-Systems/How-To/C-Offsetof-Macro

---

## Access members by name
Here is a struct:

=====[ file scope ]=====
```c
typedef struct
{
    int a;
    int b;
    int c;
} ThreeInts_s;
```
The struct is initialized with a designated initializer:

```c
int main()
{
    ...
    ThreeInts_s init = {.a=1, .b=2, .c=3};
    ...
}
```
The struct members are accessible by name:

`init.a`:1, `init.b`:2, `init.c`:3

## No language support to access members by index.
There is no index notation like for an array. And there is no language feature to query the number of members.

To access struct members by iterating in a loop, I need to know the number of members in the struct. Does `sizeof()` tell me this? No:

`sizeof(init)`: 12

## Code struct indexing yourself. But how?
`sizeof()` tells me the *number of bytes*, not the number of members. Hardcode the number of members as a macro:

```c
#define NumberOfMembers 3
```
Providing a member's index for looping is much harder.

---
The struct has an address. The structs I care about are on the heap, so this is a heap address. The address of the struct name itself is the address of the first member in the struct. Access subsequent struct members by a `base-plus-offset` approach. The idea is to calculate the address of some member since you know the address of the first member, the index of this member, and the size in bytes of every member.

The only hitch in the calculation is the system-dependent padding. That is why there is a library function that does this calculation given the struct typedef and the name of the member in question. The function is `offsetof()` in `stddef.h`. [Here is an article about offsetof][offsetof-article]. The function treats the address of the first member as zero, so the *real* address is the value returned by `offsetof` plus the base address of the struct. Here are the offsets for the three members of a ThreeInts_s: 
[offsetof-article]: https://barrgroup.com/Embedded-Systems/How-To/C-Offsetof-Macro

`offsetof(ThreeInts_s, a)`: 0,

`offsetof(ThreeInts_s, b)`: 4,

`offsetof(ThreeInts_s, c)`: 8

And get the address on the heap of the first struct member with `&`:

`(uintptr_t)&init`: 4294953960

Here are the addresses of the three members:

`(uintptr_t)&init + offsetof(ThreeInts_s, a)`: 4294953960,

`(uintptr_t)&init + offsetof(ThreeInts_s, b)`: 4294953964,

`(uintptr_t)&init + offsetof(ThreeInts_s, c)`: 4294953968

Sanity-check this matches the address obtained with the simple `&` operator on `init.a`:

`           (void *)&(init.a)`: 0xffffcbe8

`(uintptr_t)(void *)&(init.a)`: 4294953960

`        (uintptr_t)&(init.a)`: 4294953960

`(uintptr_t)&init + offsetof(ThreeInts_s, a)`: 4294953960,

Having calculated the address, access the value.
```c
int *a_addr = (int *)(uintptr_t)&init + offsetof(ThreeInts_s, a)/sizeof(int);
int *b_addr = (int *)(uintptr_t)&init + offsetof(ThreeInts_s, b)/sizeof(int);
int *c_addr = (int *)(uintptr_t)&init + offsetof(ThreeInts_s, c)/sizeof(int);
```
*a_addr: 1

*b_addr: 2

*c_addr: 3

Putting all of the above together, associate a member index with a byte offset, then add it to the base address, then dereference it. There is a subtelty in the pointer arithmetic in the units. This is why the offset is divided by the `sizeof(int)`.

## Pointer arithmetic subtelty
Here are the offsets and the expected values at each address:
```c
uintptr_t offsets[3];
offsets[0] = offsetof(ThreeInts_s, a);
offsets[1] = offsetof(ThreeInts_s, b);
offsets[2] = offsetof(ThreeInts_s, c);
```
offsets[0]: 0 bytes, init.a: 1

offsets[1]: 4 bytes, init.b: 2

offsets[2]: 8 bytes, init.c: 3

## Careful with casts in the address arithmetic.

---
Casting base as `(int *)base` does not work! `a` has the right value, but `b` and `c` do not:

```c
uintptr_t base = (uintptr_t)&init;
int *pa = (int *)base + offsets[0];
int *pb = (int *)base + offsets[1];
int *pc = (int *)base + offsets[2];
```
offsets[0]: 0, *pa: 1

offsets[1]: 4, *pb: -2145047904

offsets[2]: 8, *pc: -2147190350

Blindly thinking `(void *)` solves everything, swapping out `(int *)base` for `(void *)` seems to work, but do not do this. 

---
```c
uintptr_t base = (uintptr_t)&init;
int *pa = (void *)base + offsets[0];
int *pb = (void *)base + offsets[1];
int *pc = (void *)base + offsets[2];
```
offsets[0]: 0, *pa: 1

offsets[1]: 4, *pb: 2

offsets[2]: 8, *pc: 3

Casting base as a pointer to void only works because there is a GNU extension to treat `(void *)` as `(char *)` for pointer aritmetic. Normally pointer arithmetic is prohibited for a pointer to void. Here is the compiler warning about this:

```make
src/struct-iterating.c|267 col 23| warning: arithmetic on a pointer to void is a GNU extension [-Wpointer-arith]
||     pa = (void *)base + offsets[0];
||          ~~~~~~~~~~~~ ^
```
We are now operating at the bytes level. We know that base holds a number in units of bytes and that our offsets are in units of bytes. So the address to point to is calculated in units of bytes. 
```c
pa = (uint8_t *)base + offsets[0];
pb = (uint8_t *)base + offsets[1];
pc = (uint8_t *)base + offsets[2];
```
offsets[0]: 0, *pa: 1

offsets[1]: 4, *pb: 2

offsets[2]: 8, *pc: 3

This is physically correct, but we have confused the compiler. Casting `base` as a `(int *)` to make the compiler happy is completely wrong. The number returned by offsets is in *bytes*, not *ints*. The compiler assumes the units are ints. For `init.a` this does not matter because the offset is 0 and 0 bytes equals 0 ints. But for non-zero offset, we are pointing to the completely wrong place, in fact, `init.b` isn't even inside the `init` memory space: it is one `int` beyond.

`(uint8_t *)` is the correct casting, but it throws an incompatible types message:
```make
src/struct-iterating.c|224 col 10| warning: incompatible pointer types initializing 'int *' with an expression of type 'uint8_t *' (aka 'unsigned char *') [-Wincompatible-pointer-types]
||     int *pa = (uint8_t *)base + offsets[0];
||          ^    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```
What is to be done to silence the warning? Our pointer has to be declared as a pointer to an int because when we dereference it, the compiler needs to know how many bytes make up its value. We can go back to casting base as a pointer to int but do math on the offset to put it in units of `int`.

```c
pa = (int *)base + offsets[0]/sizeof(int);
pb = (int *)base + offsets[1]/sizeof(int);
pc = (int *)base + offsets[2]/sizeof(int);
```
offsets[0]: 0, *pa: 1

offsets[1]: 4, *pb: 2

offsets[2]: 8, *pc: 3

The only hitch is that calculating the address requires knowing the datatype. To avoid this, cast the `lhs` as void pointer?

```c
void *vpa = (uint8_t *)base + offsets[0];
void *vpb = (uint8_t *)base + offsets[1];
void *vpc = (uint8_t *)base + offsets[2];
```
offsets[0]: 0, `*(int *)vpa`: 1

offsets[1]: 4, `*(int *)vpb`: 2

offsets[2]: 8, `*(int *)vpc`: 3

Yes! That works. And it makes the pointer arithmetic simpler without the divide by sizeof() step. But we still need to know the datatype when the pointer is dereferenced.

Lastly, I am too tired to write out the mechanics, but using the above code examples, use `offsetof()` to setup an array of *offset strides* to index in the for loop.


---
Conclusion: this is a lot of work and is maybe a sign I'm doing something stupid. Is the deep copy without naming elements really necessary?

