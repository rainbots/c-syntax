# What is the syntax to make a struct member an array?
This is wrong:
```c
typedef struct
{
    int numbers[];  // wrong: need to specify array size
}IntegerListA_s;
```
clang throws this compiler error:
```make
src/arrays-in-structs.c|5 col 9| error: flexible array member 'numbers' not allowed in otherwise empty struct
||     int numbers[];
||         ^
|| 1 error generated.
```
Give the array size:
```c
typedef struct
{
    int numbers[6];  // this is right
}IntegerListA_s;
```
This is true in general, not just for struct members, though the error message is slightly different.
```c
int main()
{
    ...
    int array_needs_a_size[];  // wrong
    ...
}
```
Attempting to define an array without a size at *function scope* is a compiler error. Here is the clang error for declaring an array without a size at *function scope*:
```make
src/arrays-in-structs.c|43 col 9| error: definition of variable with array type needs an explicit size or an initializer
||     int array_needs_a_size[];
||         ^
|| 1 error generated.
```
Attempting to define an array without a size at *file scope* is a compiler warning. Here is the clang warning for declaring an array without a size at *file scope*:
```make
src/arrays-in-structs.c|7 col 5| warning: tentative array definition assumed to have one element
|| int array_needs_a_size[];
||     ^
|| 1 warning generated.
```

---
## Array size cannot be a variable.

The array size must be known at compile time, otherwise the array is considered *variable length* and clang throws a compiler warning:
```make
lib/test/test_bsort.c|37 col 18| warning: variable length array folded to constant array as an extension [-Wgnu-folding-constant]
||     int unsorted[MaxListLength];
||                  ^~~~~~~~~~~~~
```

The same warning is generated whether the array is in a struct or not, and whether the array is declared at file scope or at function scope. Here is the function scope version:
```c
int const array_size = 3;
int a_variable_array[array_size]; // wrong: warning
```
Use a `#define` to remove the warning.
```c
#define array_size 3
int not_a_variable_array[array_size]; // right
```
# What is the syntax to make a struct member a pointer?
This is similar, but there is no size specification.
```c
typedef struct
{
int *integers;
}IntegerListP_s;
```
# What is the syntax to initialize an array with a designated initializer?
## Initializing an array with `{}` works in clang and g++
Initialize an array:
```c
int integers[] = {1, 2, 3};
```
Access the first element:

---
integers[0]: 1
# What is the syntax to initialize a pointer with a designated initializer?
## Can I initialize a pointer the same way? No. Cast as an array (C only).
```c
int *intptr = {1, 2, 3};  // this is wrong
```

Make issues two warnings:
```make
/src/arrays-in-structs.c|30 col 20| warning: incompatible integer to pointer conversion initializing 'int *' with an expression of type 'int' [-Wint-conversion]
||     int *intptr = {1, 2, 3};
||                    ^
/cygdrive/c/chromation-dropbox/Dropbox/c/TddFramework/TestDrive/kata/c-syntax/src/arrays-in-structs.c|30 col 23| warning: excess elements in scalar initializer
||     int *intptr = {1, 2, 3};
||                       ^
|| 2 warnings generated.
```

The first warning tells me the compiler views `{1, 2, 3}` as an `int` rather than an *array* of `int`.
The second warning really drives home the point that clang was expecting the *rhs* to be a single value.

Solution: cast the literal as an array of int:
```c
int *intptr = (int[]){1, 2, 3};
```

Access the first element:

---
*intptr: 1

This works too:

---
intptr[0]: 1
## Casting as an array does not work in C++. Make an array first, then assign the pointer to it.
```c
int *intptr = (int[]){1, 2, 3};
```
g++ throws `error: taking address of temporary array`. And of course the executable does not build.
```make
src/arrays-in-structs.c|31 col 34| warning: ISO C++ forbids compound-literals [-Wpedantic]
||      int *intptr = (int[]){1, 2, 3};
||                                   ^
/cygdrive/c/chromation-dropbox/Dropbox/c/TddFramework/TestDrive/kata/c-syntax/src/arrays-in-structs.c|31 col 26| error: taking address of temporary array
||      int *intptr = (int[]){1, 2, 3};
||                           ^~~~~~~~~
```
To make this work in C and C++, create an array first to allocate automatic memory. Then assign the pointer.
```c
int integers[] = {1, 2, 3};
int *intptr = integers;
```
## Does any of this change if the pointer is a `struct` member? No :)
At file scope, I declare the `typedef`:
```c
typedef struct
{
    int *integers;
}IntegerListP_s;
```
And I attempt to initialize the struct in the manner that initializing the pointer to an array worked in C/C++:
```c
// Works for C/C++.
// g++ issues a designated initializer warning as expected.
int integers[] = {1, 2, 3};
IntegerListP_s list = {.integers=integers};
```

As expected from the test in [struct-init][si], g++ gives the warning that it doesn't like designated initializers, but the executable still builds under clang and g++ and produces the same output.
[si]: https://bitbucket.org/rainbots/c-syntax/src/master/build/struct-init.md
```make
/src/arrays-in-structs.c|111 col 26| warning: ISO C++ does not allow C99 designated initializers [-Wpedantic]
||      IntegerListP_s list = {.integers=integers};
||                           ^
```

Access the first element of `list.integers`:

---
list.integers[0]: 1
# Struct member as *pointer* vs *array* matters when copying the struct
A great use of structs is returning a `struct` from a function. This is a common trick for returning arrays. The `struct` contents are copied when returning, whereas a pointer is copied, but whatever it pointed to is trashed. An array cannot be returned on its own, but it can be returned as part of a `struct`.

Returning this `struct` from a function returns the array value assigned in the function:
```c
typedef struct
{
    int numbers[6];
}IntegerListA_s;

IntegerListA_s Initialize()
{ // This works.
    IntegerListA_s list = {
        {1, 2, 3, 4, 5, 6}
        };
    return list;
}
```
```c
int main()
{
...
IntegerListA_s good_list = Initialize();
...
}
```

`good_list` returned from `Initialize()`:

---
```c
{1, 2, 3, 4, 5, 6}
```

This does not extend to `struct` pointer members. A `struct` pointer is treated as a pointer: the address is copied, but whatever it points to is trashed. Copying a struct and retaining the values of what its pointer members point to requires making a *deep* copy. The `=` is just a shallow copy. Here is the same code but with a pointer member. The function creates an array and then initializes the pointer member to point at the array. The extra step is required for compiling with g++ without error. But the array pointed to the by `struct` pointer member does not contain those initial values when the `struct` is returned. Instead, a `bad_list` of random garbage values is at those addresses:
```c
typedef struct
{
    int *integers;
}IntegerListP_s;

IntegerListP_s FailsToInitialize()
{ // This does not work (on purpose -- it's an example).
    int initial_values[] = {1, 2, 3, 4, 5, 6};
    IntegerListP_s list = {
        .integers=initial_values
        };
    return list;
}
```
```c
int main()
{
...
IntegerListP_s bad_list = FailsToInitialize();
...
}
```

`bad_list` returned from `FailsToInitialize()`:

---
```c
{1, 2, -13592, 0, 5, 6}
```
