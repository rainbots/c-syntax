# What is the syntax to initialize a struct?
The real question ended up being
*Does C++ support designated initializers?* No.
## A simple `struct` type definition:
```c
typedef struct
{
    int a;
}IntegerList_s;
```
## This works in clang:

Define and initialize `list`:
```c
IntegerList_s list = {.a=1};
```
`list.a`: 1
## But g++ issues a warning:
```make
src/struct-init.c|12 col 26| warning: ISO C++ does not allow C99 designated initializers [-Wpedantic]
||      IntegerList_s list = {.a=1};
||                           ^
```
This is not a shock.
C supports designated initializers.
C++ does not.
But somehow it only triggers a warning, so I can still compile in C++.
Ben Klemens [stands behind designated initializers][bk-des-inits-rock]:
[bk-des-inits-rock]: https://modelingwithdata.org/pdfs/030-c_v_cpp.pdf

> On the other side, the ISO added a few features to C a decade ago. The most notable
> for me is designated initializers; I’ve written several entries here about how much you
> can get out of this syntactic tweak. However, C++ has no intention of supporting
> them. This author3
> feels the rationale paper for not using designated initializers gives
> “arguments that aren’t very convincing”, and I’d agree.
